Simple steps to be followed in order to create a simple crud operation with Django framework:

1. Install Django framework
2. Go to command prompt and execute below command : 
    django startproject {project_name}
3. To generate default database tables (By default it uses sql-lite database) use below command. Db configuration can be changed from settings.py file.
     python3 manage.py makemigrations  
     
4. In order to import any new custom database table use below command.
     python3 manage.py migrate  

5. To run server. Please use below command:
    python3 manage.py runserver

Note: Here python3 is virtual environment.

